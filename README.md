# automated-jitsi

Simple Terraform files to create and deploy a DigitalOcean droplet + firewall, and an AWS Route 53 DNS record pointing to it.

## Rename and edit hcl and tfvars

```shell
$ cp secrets/example.hcl secrets/remoteTfState.hcl
$ cp secrets/example.tfvars secrets/jitsi.tfvars
```

## Export variables

```shell
$ export HISTCONTROL=ignorespace
$  export AWS_ACCESS_KEY_ID=
$  export AWS_SECRET_ACCESS_KEY=
$  export DIGITALOCEAN_TOKEN=
```

## To get DigitalOcean slugs

Make API requests with curl and export them to json:

```shell
curl -X GET -H "Content-Type: application/json" "https://api.digitalocean.com/v2/images?type=distribution" \            
        -H "Authorization: Bearer $DIGITALOCEAN_TOKEN" | python -m json.tool > images.json
```

You can also use [doctl](https://github.com/digitalocean/doctl)

```shell
$ doctl compute region list
Slug    Name               Available
nyc1    New York 1         true
sgp1    Singapore 1        true
lon1    London 1           true
nyc3    New York 3         true
ams3    Amsterdam 3        true
fra1    Frankfurt 1        true
tor1    Toronto 1          true
sfo2    San Francisco 2    true
blr1    Bangalore 1        true
sfo3    San Francisco 3    true
```

Or check [this website](https://slugs.do-api.dev/)

## To init Terraform remote backend (AWS S3)

```shell
terraform init -backend=true -backend-config=secrets/remoteTfState.hcl -reconfigure -upgrade=true
```

## To Terraform plan/apply

```shell
terraform apply -var-file=secrets/jitsi.tfvars
```

## To Terraform destroy

```shell
terraform destroy -var-file=secrets/jitsi.tfvars
```
