## AWS
aws_tfstate_bucket = "bucketWithMyTfStates"     # Name of the S3 bucket
aws_tfstate_key    = "folder/terraform.tfstate" # Folder and file name inside the bucket
aws_region         = "us-west-1"                # AWS region
route53_zone       = "example.com."             # AWS Route 53 zone. Must end with a dot.


## Digital Ocean
# Common
tags          = ["Tag1", "Tag2"]           # A list of tags to insert into resources
ssh_publickey = "/path/to/ssh/pub/key.pub" # Path to a public RSA key

# Droplet
image     = "ubuntu-18-04-x64" # Base OS image to deploy
name      = "MyDroplet"        # Name of the droplet
do_region = "blr1"             # Slug for the region
size      = "s-1vcpu-1gb"      # Slug for the size

# Firewall
allowed_ssh_source = ["0.0.0.0/0"] # Allowed source IP to access with SSH
