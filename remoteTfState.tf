terraform {
  backend "s3" {
    bucket = var.aws_tfstate_bucket
    key    = var.aws_tfstate_key
    region = var.aws_region
  }
}
