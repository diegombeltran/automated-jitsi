# AWS Variables
variable "aws_tfstate_bucket" {
  type        = string
  description = "Bucket containing remote Terraform tfstates"
}

variable "aws_tfstate_key" {
  type        = string
  description = "Path inside the bucket pointing to tfstate file"
}

variable "aws_region" {
  type        = string
  description = "AWS Region where resources live"
}

variable "route53_zone" {
  type        = string
  description = "Route 53 zone name to look for"
}

# DigitalOcean
variable "tags" {
  type        = list(string)
  description = "A list of tags separated by comas"
}

variable "ssh_publickey" {
  type        = string
  description = "SSH public key path to access the droplet"
}

variable "image" {
  type        = string
  description = "Slug for the DigitalOcean base OS image"
}

variable "name" {
  type        = string
  description = "Name of the droplet"
}

variable "do_region" {
  type        = string
  description = "Slug for DigitalOcean region"
}

variable "size" {
  type        = string
  description = "Slug for the DigitalOcean droplet size"

}

variable "allowed_ssh_source" {
  type        = list(string)
  description = "A list of sources allowed to SSH connect"

}
