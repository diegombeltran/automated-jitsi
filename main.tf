resource "digitalocean_vpc" "jitsi" {
  name   = var.name
  region = var.do_region
}

resource "digitalocean_firewall" "jitsi" {

  name        = var.name
  tags        = var.tags
  droplet_ids = [digitalocean_droplet.jitsi.id]

  inbound_rule {
    protocol         = "tcp"
    port_range       = "22"
    source_addresses = var.allowed_ssh_source
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "80"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "4443"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  inbound_rule {
    protocol         = "tcp"
    port_range       = "10000"
    source_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "tcp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "udp"
    port_range            = "1-65535"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }

  outbound_rule {
    protocol              = "icmp"
    destination_addresses = ["0.0.0.0/0", "::/0"]
  }
}

resource "digitalocean_ssh_key" "jitsi" {
  name       = var.name
  public_key = file(var.ssh_publickey)
}

resource "digitalocean_droplet" "jitsi" {
  image    = var.image
  name     = var.name
  region   = var.do_region
  size     = var.size
  vpc_uuid = digitalocean_vpc.jitsi.id
  ssh_keys = [digitalocean_ssh_key.jitsi.fingerprint]
}

data "aws_route53_zone" "route53_zone" {
  name = var.route53_zone
}

resource "aws_route53_record" "jitsi" {
  zone_id = data.aws_route53_zone.route53_zone.zone_id
  name    = "jitsi.${data.aws_route53_zone.route53_zone.name}"
  type    = "A"
  ttl     = "300"
  records = [digitalocean_droplet.jitsi.ipv4_address]
}

